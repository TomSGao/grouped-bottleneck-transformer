import torch
from torch.nn.modules import dropout
from tqdm import tqdm
from einops.layers.torch import Rearrange
from typing import Optional, Sequence
from torch import Tensor
from torch import nn
from torch.nn import functional as F
import numpy as np


class BNReLU(torch.nn.Module):

    def __init__(
        self, 
        out_channels, 
        activation='relu', 
        nonlinearity=True, 
        init_zero=False
    ):
        super().__init__()
        self.norm = torch.nn.BatchNorm3d(
            out_channels, 
            momentum=0.9, 
            eps=1e-5
        )
        if nonlinearity:
            self.act = get_act(activation)
        else:
            self.act = None
        
        if init_zero:
            torch.nn.init.constant_(self.norm.weight, 0)
        else:
            torch.nn.init.constant_(self.norm.weight, 1)

    def forward(self, input):
        out = self.norm(input)
        if self.act is not None:
            out = self.act(out)
        return out


class GroupPointWise(torch.nn.Module):

    def __init__(
        self, 
        in_channels, 
        heads=4, 
        proj_factor=1, 
        target_dimension=None
    ):
        super().__init__()
        if target_dimension is not None:
            proj_channels = target_dimension // proj_factor
        else:
            proj_channels = in_channels // proj_factor
        self.w = torch.nn.Parameter(
            torch.Tensor(in_channels, heads, proj_channels // heads)
        )
        torch.nn.init.normal_(self.w, std=0.01)
    
    def forward(self, input):
        input = input.permute(0, 2, 3, 4, 1)
        out = torch.einsum('bdhwc,cnp->bndhwp', input, self.w)
        return out
    

class RelPosSelfAttention(torch.nn.Module):

    def __init__(self, d, h, w, dim, relative=True, fold_heads=False):
        super().__init__()
        self.relative = relative
        self.fold_heads = fold_heads
        self.rel_emb_d = torch.nn.Parameter(torch.Tensor(2 * d - 1, dim))
        self.rel_emb_w = torch.nn.Parameter(torch.Tensor(2 * w - 1, dim))
        self.rel_emb_h = torch.nn.Parameter(torch.Tensor(2 * h - 1, dim))
        torch.nn.init.normal_(self.rel_emb_d, std=dim ** -0.5)
        torch.nn.init.normal_(self.rel_emb_w, std=dim ** -0.5)
        torch.nn.init.normal_(self.rel_emb_h, std=dim ** -0.5)
    
    def forward(self, q, k, v):
        bs, heads, dep, h, w, dim = q.shape
        q = q * (dim ** -0.5)
        logits = torch.einsum('bnihwd,bnjpqd->bnihwjpq', q, k)
        if self.relative:
            logits += self.relative_logits(q)
        weights = torch.reshape(logits, [-1, heads, dep, h, w, dep * h * w])
        weights = torch.nn.functional.softmax(weights, dim=-1)
        weights = torch.reshape(weights, [-1, heads, dep, h, w, dep, h, w])
        attn_out = torch.einsum('bnihwjpq,bnjpqd->bihwnd', weights, v)
        if self.fold_heads:
            attn_out = torch.reshape(attn_out, [-1, dep, h, w, heads * dim])
        return attn_out

    def rel_to_abs(self, x):
        '''
        Input: [bs, heads, length, 2*length - 1]
        Output: [bs, heads, length, length]
        '''
        bs, heads, length, _ = x.shape
        device = x.device
        col_pad = torch.zeros((bs, heads, length, 1), dtype=x.dtype).to(x.device)
        x = torch.cat([x, col_pad], dim=3)
        flat_x = torch.reshape(x, [bs, heads, -1]).to(x.device)
        flat_pad = torch.zeros((bs, heads, length - 1), dtype=x.dtype).to(x.device)
        flat_x_padded = torch.cat([flat_x, flat_pad], dim=2)
        final_x = torch.reshape(
            flat_x_padded, [bs, heads, length + 1, 2 * length - 1])
        final_x = final_x[:, :, :length, length - 1:]
        return final_x
    
    def relative_logits_1d(self, q, rel_k, transpose_mask):
        bs, heads, dep, h, w, dim = q.shape
        # bs head h w dep dim
        rel_logits = torch.einsum('bhixyd,md->bhixym', q, rel_k)
        # bs head h w dep m=2dep-1
        rel_logits = torch.reshape(rel_logits, [-1, heads * dep * h, w, 2 * w - 1])
        # bs (head h w) dep m
        rel_logits = self.rel_to_abs(rel_logits)
        # bs (head h w) dep dep
        rel_logits = torch.reshape(rel_logits, [-1, heads, dep, h, w, w])
        # bs head h w dep dep
        rel_logits = torch.unsqueeze(rel_logits, dim=3)
        rel_logits = torch.unsqueeze(rel_logits, dim=5)
        # bs head h 1 w 1 dep dep
        rel_logits = rel_logits.repeat(1, 1, 1, dep, 1, h, 1, 1)
        # bs head h h w w dep dep 
        rel_logits = rel_logits.permute(*transpose_mask)
        return rel_logits
    
    def relative_logits(self, q):
        # Relative logits in width dimension.
        # bs, heads, dim, h, w, dim = q.shape
        rel_logits_w = self.relative_logits_1d(q, self.rel_emb_w, transpose_mask=[0, 1, 2, 4, 6, 3, 5, 7])
        # Relative logits in height dimension
        rel_logits_h = self.relative_logits_1d(q.permute(0, 1, 4, 2, 3, 5), self.rel_emb_h,
                                               transpose_mask=[0, 1, 4, 6, 2, 5, 7, 3])
        rel_logits_d = self.relative_logits_1d(q.permute(0, 1, 3, 4, 2, 5), self.rel_emb_d,
                                               transpose_mask=[0, 1, 6, 2, 4, 7, 3, 5])
        return rel_logits_h + rel_logits_w + rel_logits_d
    

class MHSA(torch.nn.Module):

    def __init__(
        self,
        in_channels,
        heads,
        curr_d,
        curr_h,
        curr_w,
        pos_enc_type='relative', 
        use_pos=True
    ):
        super().__init__()
        self.q_proj = GroupPointWise(in_channels, heads, proj_factor=1)
        self.k_proj = GroupPointWise(in_channels, heads, proj_factor=1)
        self.v_proj = GroupPointWise(in_channels, heads, proj_factor=1)
        assert pos_enc_type in ['relative', 'absolute']
        if pos_enc_type == 'relative':
            self.self_attention = RelPosSelfAttention(curr_d, curr_h, curr_w, in_channels // heads, fold_heads=True)
        else:
            raise NotImplementedError
    
    def forward(self, input):
        q = self.q_proj(input)
        k = self.k_proj(input)
        v = self.v_proj(input)
        o = self.self_attention(q=q, k=k, v=v)
        return o


class BotBlock(torch.nn.Module):

    def __init__(
        self,
        in_dimension,
        curr_d,
        curr_h,
        curr_w,
        num_mhsa=1,
        nhead=4,
        q=4,
        proj_factor=4,
        activation='relu',
        pos_enc_type='relative',
        target_dimension=None,
        dropout_rate=0.2
    ):
        super().__init__()
        self.q = q
        self.shortcut = torch.nn.Sequential(
            torch.nn.Conv3d(
                in_dimension, target_dimension, 
                kernel_size=3, padding=1, stride=1
            ),
            BNReLU(target_dimension)
        )
        bottleneck_dimension = target_dimension // proj_factor
        self.conv1 = torch.nn.Sequential(
            torch.nn.Conv3d(
                in_dimension, bottleneck_dimension, 
                kernel_size=3, padding=1, stride=1
            ),
            BNReLU(bottleneck_dimension)
        )
        if num_mhsa == 1:
            self.mhsa = MHSA(
                in_channels=bottleneck_dimension, heads=nhead, 
                curr_d=curr_d, curr_h=curr_h, curr_w=curr_w,
                pos_enc_type=pos_enc_type
            )
        else:
            self.mhsa = torch.nn.Sequential(
                *[
                    torch.nn.Sequential(
                        MHSA(
                            in_channels=bottleneck_dimension, heads=nhead, 
                            curr_d=curr_d, curr_h=curr_h, curr_w=curr_w,
                            pos_enc_type=pos_enc_type
                        ), 
                        Rearrange('n d h w c -> n c d h w')
                    ) for _ in range(num_mhsa - 1)
                ] + \
                [
                    MHSA(
                        in_channels=bottleneck_dimension, heads=nhead, 
                        curr_d=curr_d, curr_h=curr_h, curr_w=curr_w,
                        pos_enc_type=pos_enc_type
                    )
                ]
            )
        self.bnrelu = BNReLU(bottleneck_dimension)
        self.conv2 = torch.nn.Sequential(
            torch.nn.Conv3d(
                bottleneck_dimension, target_dimension,
                kernel_size=3, stride=1, padding=1
            ),
            BNReLU(target_dimension, init_zero=True)
        )
        self.dropout = False
        if dropout_rate:
            self.dropout = True
        self.dropout_layer = torch.nn.Dropout(dropout_rate)

        self.last_act = get_act(activation)
    
    def forward(self, x):
        shortcut = self.shortcut(x)
        Q_d = Q_h = Q_w = self.q
        N, C, D, H, W = x.shape
        P_d, P_h, P_w = D // Q_d, H // Q_h, W // Q_w
        x = x.reshape(N * P_d * P_h * P_w, C, Q_d, Q_h, Q_w)
        out = self.conv1(x)
        out = self.mhsa(out)
        out = out.permute(0, 4, 1, 2, 3)
        out = self.bnrelu(out)
        out = self.conv2(out)
        N1, C1, D1, H1, W1 = out.shape
        out = out.reshape(N, C1, int(D), int(H), int(W))
        out += shortcut
        if self.dropout:
            out += self.dropout_layer(out)
        out = self.last_act(out)
        return out


class GTEncoderSmall(torch.nn.Module):

    def __init__(self, input_channel=1):
        # input 32**3
        super().__init__()
        self.pool = torch.nn.MaxPool3d(2, 2)
        # 32, 4**3 windows
        self.l1 = BotBlock(1, 4, 4, 4, 4, 1, 4, 4, 'relu', 'relative', 32, 0.2)
        # 16, 4**3 windows
        self.l2 = torch.nn.Sequential(
            BotBlock(32, 4, 4, 4, 1, 4, 4, 4, 'relu', 'relative', 64, 0.2),
        )
        # 8, 2**3 windows
        self.l3 = torch.nn.Sequential(
            BotBlock(64, 4, 4, 4, 1, 4, 4, 4, 'relu', 'relative', 128, 0.2),
        )
        # 4, 1**3 windows
        # self.l4 = BotBlock(128, 4, 4, 4, 1, 4, 4, 4, 'relu', 'relative', 256, 0.2)
    
    def forward(self, x):
        x = self.l1(x)
        x = self.pool(x)
        x = self.l2(x)
        x = self.pool(x)
        x = self.l3(x)
        # x = self.pool(x)
        # x = self.l4(x)
        return x


class GTEncoderLarge(torch.nn.Module):

    def __init__(self, input_channel=1):
        # input 32**3
        super().__init__()
        self.pool = torch.nn.MaxPool3d(2, 2)
        # 32, 4**3 windows
        self.l1 = BotBlock(1, 4, 4, 4, 1, 1, 4, 4, 'relu', 'relative', 64, 0.2)
        # 16, 4**3 windows
        self.l2 = torch.nn.Sequential(
            BotBlock(64, 4, 4, 4, 1, 2, 4, 4, 'relu', 'relative', 128, 0.2),
        )
        # 8, 2**3 windows
        self.l3 = torch.nn.Sequential(
            BotBlock(128, 4, 4, 4, 1, 4, 4, 4, 'relu', 'relative', 256, 0.2),
        )
        # 4, 1**3 windows
        self.l4 = BotBlock(256, 4, 4, 4, 1, 4, 4, 4, 'relu', 'relative', 256, 0.2)
    
    def forward(self, x):
        x = self.l1(x)
        x = self.pool(x)
        x = self.l2(x)
        x = self.pool(x)
        x = self.l3(x)
        x = self.pool(x)
        x = self.l4(x)
        return x


class ClassifierHead(torch.nn.Module):

    def __init__(self, ic, hc, oc, dropout_rate=0.5):
        super().__init__()
        self.pool = torch.nn.AdaptiveMaxPool3d(1)
        self.l1 = torch.nn.Linear(ic, hc)
        self.bn = torch.nn.BatchNorm1d(hc)
        self.relu = torch.nn.ReLU()
        self.dropout = torch.nn.Dropout(dropout_rate)
        self.l2 = torch.nn.Linear(hc, oc)
    
    def forward(self, x):
        o = self.pool(x).squeeze(-1).squeeze(-1).squeeze(-1)
        o = self.l1(o)
        o = self.bn(o)
        o = self.relu(o)
        o = self.dropout(o)
        o = self.l2(o)
        return o
